

import java.sql.DriverManager

fun main(args: Array<String>) {
    println("Hello World!")

    // Try adding program arguments via Run/Debug configuration.
    // Learn more about running applications: https://www.jetbrains.com/help/idea/running-applications.html.
    println("Program arguments: ${args.joinToString()}")

    // SIMPLE SQL Query
    val connection = DriverManager.getConnection('jdbc:mysql://localhost:5432/escuela')
    connection.createStatement().use{ stmt ->
        val query = "SELECT name,lastName,birthday FROM alumons"
        stmt.executeQuery(query).use{rs ->
            while(rs.next()){
                val nombre = rs.getString('name')
                val apellido = rs.getString('lastName')
                val  bornDate = rs.getString('birthday')
                println("$nombre $apellido nacio en $bornDate")
            }
        }
    }
}